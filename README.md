# flash_chat

### static keyWord

static allows us to create class-wide variables and methods.

```
void main() {
    print(Icosagon.numberOfSides);

    Circle.workoutCircumference(radius: 15.6);
}

class Icosagon {
    static int numberofSides = 20;
}

class Circle {
    static const double pi = 3.1415926;

    static void workOutCircumference({double radius}) {
        double circumference = 2 * pi * radius;
        print(circumference);
    }
}
```

# Flutter Animation

## Hero Widget Animation

To implement hero animation all we need 3 ingredients.

- 2 x hero Widgets
- A shared tag property 
- Navigator based Screen transitions

## Custom Animation

- A ticker
- An animation controller
- An animation Value

**vsync** -> this is where we provide the tiker provider. Usualy the ticket provider going to be our state object.

```
  AnimationController controller;
  Animation animation;

  @override
  void initState() {
    super.initState();

    
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 3),
    );
  }
```

when we are applying curved animation, we cant have an upper bound in controller graeter than one.


```
  AnimationController controller;
  Animation animation;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 3),
    );

    animation = CurvedAnimation(parent: controller, curve: Curves.decelerate);
  }
```

Get the animation start -> this is going to proceed our animation forward
By defalut animation controller animate a number, every tick of the ticker will encrease that number, and the number usually goes from 0 to 1.

```
controller.forward();
```

```
controller.reverse(from 1.0);
```

The end of the fowrard animation is "completed". End of the reverse animation is "dimissed".

```
animation.addStatusListener((status) {
       if (status == AnimationStatus.completed)
         controller.reverse(from: 1.0);
       else if (status == AnimationStatus.dismissed) controller.forward();
 });
```

It is import to dispose the animation so that it does not cost memory even
   after awitching the screen
```
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
```

# Mixins

Mixins are a way of reusing a class's code in multiple classe hierarchies.

```
void main() {
  // Animal().move();
  // Fish().move();
  Duck().
}

class Animal{
  void move() {
    print("changed positons");
  }
}

class Fish extends Animal {
  @override
  void move() {
    super.move();
    print("by swimming");
  }
}

class Bird extends Animal {
  @override
  void move() {
    super.move();
    print("by flying");
  }
}

mixin CanSwim {
  void swim() {
    print("changing position by swimming");
  }
}

mixin CanFly {
  void fly() {
    print("changing position by flying");
  }
}

class Duck extends Animal with CanSwim, CanFly {
  
}
```